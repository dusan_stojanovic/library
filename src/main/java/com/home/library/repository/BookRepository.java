package com.home.library.repository;

import java.util.List;

import com.home.library.model.Book;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book,Integer> {
    public List<Book> findByBookName(String bookName);
}
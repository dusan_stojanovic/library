package com.home.library.service;

import java.util.List;

import com.home.library.model.Book;
import com.home.library.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
    
    @Autowired
    BookRepository br;

    // Get Single Book
    public Book getBook(int bookId) {
        return br.findById(bookId).get();
    }

    // Get All Books
    public List<Book> getAllBooks() {
        return (List<Book>)br.findAll();
    }

    // Get Books by Name
    public List<Book> getAllBooksByName(String bookName) {
        return br.findByBookName(bookName);
    }

    // Update/Create Book
    public Book saveBook(Book book) {
        return br.save(book);
    }

    // Delete Book
    public void deleteBook(Book book) {
        br.delete(book);
    }

}
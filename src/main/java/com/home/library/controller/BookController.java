package com.home.library.controller;

import java.util.List;

import com.home.library.model.Book;
import com.home.library.service.BookService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/api/book")
public class BookController {

    @Autowired
    BookService bs;

    // Get Single Book
    @GetMapping("/get/{id}")
    public Book get(@PathVariable("id") int bookId) {
        return bs.getBook(bookId);
    }

    // Get All Books
    @GetMapping("/get/all")
    public List<Book> getAll() {
        return bs.getAllBooks();
    }

    // Insert Book
    @PostMapping("/add")
    public Book addBook(@RequestBody Book book) {
        return bs.saveBook(book);
    }

    // Update Book
    @PutMapping("/edit")
    public Book editBook(@RequestBody Book book) {
        return bs.saveBook(book);
    }

    // Delete Book
    @DeleteMapping("/delete")
    public void delete(@RequestParam(value = "id") int bookId) {
        System.out.println("Id: " + bookId);
        Book book = bs.getBook(bookId);
        bs.deleteBook(book);
    }

}